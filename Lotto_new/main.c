//
//  main.c
//  Lotto_new
//
//  Created by Lukas Klausnitzer on 18.02.20.
//  Copyright © 2020 Lukas Klausnitzer. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/* Zahl im Array? */
int searchNumber(int * pa, int n, int border)
{
    int i;
    for (i=0;i<border;i++) {
        if (pa[i] == n) return 1;
    }
    return 0;
}

/*Sortier Hilfsfunktionen:*/
int arrayMinIndex(int *x, int start, int size) {
    int minIndex=start, i;
    for(i=start; i<size; i++) {
        if(x[i] < x[minIndex]) {
            minIndex = i;
        }
    }
    return minIndex;
}
void arraySort(int *x, int size) {
    int i, temp, minIndex;
        
    for(i=0; i<size-1; i++) {
        minIndex = arrayMinIndex(x, i, size);
        if(i != minIndex) {
        temp = x[i];
        x[i] = x[minIndex];
        x[minIndex] = temp;
        }
    }
}

void tippArray(int *b, int tipp)
{
    int i, n, f;

    printf("\nWähle und gebe 6 aus 49 Zahlen, wie folgt ein: \"1 2 3 4 5 6\": ");
    for(i = 0,f=1; i < tipp; i++,f++){
        scanf("%d", &n);
        if (n < 1 || n > 49) {
            printf("\nFalsche Eingabe: außerhalb des Bereiches von 1...49!\nVersuch es noch einmal: ");
            i--;
            f--;
        } else if (searchNumber(&b[0], n, f)) {
            printf("\nFalsche Eingabe: keine doppelten Zahlen eingeben!\nVersuch es noch einmal: ");
            i--;
            f--;
        } else {
            *(b+i) = n;
        }
    }
    for(b = 0; i < tipp; b++)
    {
        printf("%d\t", b[i]);
    }
}

void lottoArray(int * a, int max) {
    int *hilf;
    int g, h;

    hilf = a;
    if (a == NULL) {
        printf("\nNicht genuegend Speicher!");
        exit(1);
    }
    printf("\nAlle %i Lottozahlen erfolgreich initialisiert!\n", max);
    for (g=0; g<max; g++) {
        *hilf++=1+g;
    }
    for (g=0,h=1; g<max; g++,h++) {
        printf("\t%d ", a[g]);
        if (h == 7) {
            h=0;
            printf("\n");
        }
    }
}

/* Lottozahlen "ankreuzen" */
void searchTippIndezes(int * a, int * b, int max, int tipp)
{
    int i, j;

    for (i=0,j=0; i < max; i++)
    {
        a[i]=1+i;
        if (searchNumber(&a[i], b[j], max))
        {
        }
        else
        {
            a[i-1]=0;
            j++;
            if (j == tipp) {
                break;
            }
        }
    }
}
void printMatrix(int * a, int max) {
    int g,h;

    printf("\nDeine Lottotipps wurden mit einer 0 markiert: \n");
    for (g=0,h=1; g<max; g++,h++) {
        printf("\t%d ", a[g]);
        if (h == 7) {
            h=0;
            printf("\n");
        }
    }
}
void tippArrayRandom (int *array, int tipp, int max) {

    int i, n;
    for (i=0,n=1; i < tipp;) {
        int num = (rand() % (max - 1 + 1)) + 1;
//        printf("%d ", num);
        if (searchNumber(&array[0], num, n))
        {
//            printf("\nDoppelten an Pos.%d gefunden: %d\n",i,array[i]);
        } else {
            array[i]=num;
            i++;
            n++;
        }
    }
}
int pullLottoNumbers (int * z, int * p1,int * p2,int * p3,int * p4, int tipp, int rand_value, int hauptgewinn) {
    
//    printf("\nBegrenzung für random values = %d",rand_value);
    int i, n, w;
    int alleGewinne = hauptgewinn;
    const int namesize = 12;
    char *name1 = (char*) malloc(namesize);
    char *name2 = (char*) malloc(namesize);
    char *name3 = (char*) malloc(namesize);
    char *name4 = (char*) malloc(namesize);
    /*Zähler für Siege:*/
    int c1 = 0;
    int c2 = 0;
    int c3 = 0;
    int c4 = 0;
    /*seed random numbers:*/
    srand(time(0));
    printf("\n-----------------------------------\n");
    printf("Starte mit Ziehung der Lottozahlen:\n");
    for (i = 0,n=1; i < tipp; i++,n++)
    {
        int random6 = (rand() % (tipp - 1 + 1));
        int random4 = (rand() % (rand_value - 1 + 1)) + 1;
//        printf("\nrandom 4 value = %d\n",random4);
//        printf("\nrandom 6 value = %d\n",random6);
        switch(random4)
        {
            case 1:
                if (searchNumber(&z[0], p1[random6], n))
                {
                    random4 = 2;
                }
                else
                {
                    z[i]=p1[random6];
                }
                break;
            case 2:
                if (searchNumber(&z[0], p2[random6], n))
                {
                    random4 = 3;
                }
                else
                {
                    z[i]=p2[random6];
                }
                break;
            case 3:
                if (searchNumber(&z[0], p3[random6], n))
                {
                    random4 = 4;
                }
                else
                {
                    z[i]=p3[random6];
                }
                break;
            case 4:
                if (searchNumber(&z[0], p4[random6], n))
                {
                    random4 = 1;
                }
                else
                {
                    z[i]=p4[random6];
                }
                break;
        }
        if (searchNumber(&p1[0], z[i], tipp)) {
            strcpy(name1, " Spieler 1");
            c1++;
        }
        if (searchNumber(&p2[0], z[i], tipp)) {
            strcpy(name2, " Spieler 2");
            c2++;
        }
        if (searchNumber(&p3[0], z[i], tipp)) {
            strcpy(name3, " Spieler 3");
            c3++;
        }
        if (searchNumber(&p4[0], z[i], tipp)) {
            strcpy(name4, " Spieler 4");
            c4++;
        }
        if (z[i] == 0) {
            i--;
            n--;
        } else {
            printf("%d.Ziehung: %d\t Treffer:%s%s%s%s \n",i+1, z[i],name1,name2,name3,name4);
        }
        /*Lösche alle Zeichen:*/
        *name1 = '\0';
        *name2 = '\0';
        *name3 = '\0';
        *name4 = '\0';
    }
    printf("Ende mit Ziehung der Lottozahlen.\n");
    printf("---------------------------------\n");
    printf("\nAuswertung:\n");
    for (w=7; w>0; w--) {
        if (c1 == w) {
            printf("Spieler 1 hat %dx richtig getippt!\n",w);
            if (c1 == 6) {
                hauptgewinn++;
            }
        }
        if (c2 == w) {
            printf("Spieler 2 hat %dx richtig getippt!\n",w);
        }
        if (c3 == w) {
            printf("Spieler 3 hat %dx richtig getippt!\n",w);
        }
        if (c4 == w) {
            printf("Spieler 4 hat %dx richtig getippt!\n",w);
        }
    }
    if (hauptgewinn != alleGewinne) {
        printf("\n ******************************************************************");
        printf("\n \t*\t*\t*\t\t\t\t*\t*\t*\t*\t\t*\t\t\t*\t*");
        printf("\n *\t*\t*\t\t*\t*\t\t\t*\t**\t*\t\t*");
        printf("\n Glückwunsch du hast 6 richtige getippt und den Hauptgewinn erzielt!");
        printf("\n *\t*\t\t*\t*\t\t\t*\t\t*\t*\t*\t*");
        
        printf("\n \t*\t\t*\t\t\t*\t*\t*\t\t*\t**\t*");
        printf("\n ******************************************************************");
    }
    return hauptgewinn;
}
int printArray(int *x, int rows, int cols, int player) {
    int i, j;
    player += 1;
    printf(" Spieler %d:",player);

    for(i=0; i<rows; i++) {
        for(j=0; j<cols; j++) {
            printf(" %d",*(x+(i*cols)+j));
        }
        printf("\n");
    }
    return player;
}
int main(int argc, const char * argv[])
{
    /*Zähler für Mitspieler:*/
    int player;
    /*Gewonnen:*/
    int hauptgewinn = 0;
    int programmdurchlauf = 1;
    int run = 1;
    int rand_value = 4;
    const int max = 49;
    const int tippsize = 6;
    printf("------------------------------------------------------------------\n");
    printf("\t\tWillkommen beim Lottospiel 6 aus 49\n\n");
    printf("\t\t\t******Hinweis******\n");
    printf("\tIn diesem Spiel tippt einer von vier Spielern\n");
    printf("\t\tgarantiert mindestens einmal richtig.\n");
    printf("\tSchaffst du es einen 6er im Lotto zu bekommen?\n");
    printf("------------------------------------------------------------------\n");
    while (run!=0)
    {
        int a[max] = {};
        int b[tippsize] = {};
        int c[tippsize] = {};
        int d[tippsize] = {};
        int e[tippsize] = {};
        int ziehung[tippsize] = {};
        lottoArray(&a[0], max); // lotto array of 49
        tippArray(&b[0], tippsize); // 6 Zahlen Array von 49
        arraySort(&b[0], tippsize); // Sortiere die eingegebenen Werte von kleinste nach größte
        player = 0; // Setzt Anzahl der Spieler zurück
        printf("\nDu bist");
        player = printArray(&b[0], 1,6,player);
        searchTippIndezes(&a[0], &b[0], max, tippsize);
        printMatrix(&a[0], max);
        
        printf("Die anderen Spieler tippten wie folgt: \n");
        tippArrayRandom(&c[0], tippsize, max);
        arraySort(&c[0], tippsize);
        player = printArray(&c[0], 1,6,player);
        tippArrayRandom(&d[0], tippsize, max);
        arraySort(&d[0], tippsize);
        player = printArray(&d[0], 1,6,player);
        tippArrayRandom(&e[0], tippsize, max);
        arraySort(&e[0], tippsize);
        player = printArray(&e[0], 1,6,player);

        hauptgewinn = pullLottoNumbers(&ziehung[0],&b[0],&c[0],&d[0],&e[0],tippsize,rand_value,hauptgewinn);
        
        printf("\nDu hast bisher %d mal gespielt und %d mal den Hauptgewinn erzielt.",programmdurchlauf, hauptgewinn);
        printf("\nWillst du noch einmal spielen?");
        printf("\n(1 = Ja / 0 = Nein)");
        printf("\nDeine Eingabe: ");
        scanf("%d", &run);

        switch (run)
        {
            case 1:
                programmdurchlauf++;
                if (rand_value != 1) {
                    rand_value--;
                } else if (rand_value == 1) {
                    rand_value = 4;
                }
                break;
            default:
                break;
        }
    }
        
    return 0;
}
