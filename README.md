# README #

### C Projekt für das 5. Semester ###

- Lotto Programm in C
- Version 0.1

#### Programmierer
- Lukas Klausnitzer

#### Installationsanweisung
- main Hauptfunktion befindet sich in der main.c im Ordner: "Lotto_new"
- Start des Programs durch Lotto_new.exec mittels doppelklick auf ausführbare unix - Datei im Ordner: "Lotto_new"
- gesamte Ordnerstruktur kann in macOS als xcode Projekt eingebunden werden
- Ausführung getestet unter macOS Catalina 10.15.3
- bei anderen Betriebssystemen muss mittels GNU Compiler Collection eine beliebig ausführbare Datei von main.c erstellt werden

#### Regeln
- nur Zahlen zwischen 1 bis 49 pro Tipp erlaubt
- keine Eingabe von doppelten Zahlen möglich

#### Siegbedingungen
-  Mit einer Wahrscheinlichkeit von ca. 25% gewinnt Spieler 1 bis 4 bis zur 3. Runde
-  Nach je 4 Spielrunden gewinnt immer Spieler 1

#### Bekannte Fehler
- nach Eingabeaufforderungen: bei Zeicheneingaben gelangt das Programm in eine Dauerschleife
